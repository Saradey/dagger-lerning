package com.example.googlemaplerning.evgeny.daggerlerning

import android.app.Application
import android.content.Context
import com.example.googlemaplerning.evgeny.daggerlerning.di.AppComponent
import com.example.googlemaplerning.evgeny.daggerlerning.di.DaggerAppComponent
import com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components.ActivitySubComponent
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepository
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepositoryImpl
import javax.inject.Inject

class App : Application() {


    companion object {
        lateinit var appComponent: AppComponent
    }

    @Inject
    lateinit var repository: CarRepository

    @Inject
    lateinit var appContext: Context



    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .appContext(this)
            .carsRepository(CarRepositoryImpl())
            .build()

        appComponent.inject(this)
    }


}