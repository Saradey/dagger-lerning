package com.example.googlemaplerning.evgeny.daggerlerning.di

import com.example.googlemaplerning.evgeny.daggerlerning.App
import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.AppModule
import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.BindBuilderSubcomponentsModule
import com.example.googlemaplerning.evgeny.daggerlerning.di.scopes.ActivityScopes
import com.example.googlemaplerning.evgeny.daggerlerning.di.scopes.AppScopes
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepositoryImpl
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainActivity
import dagger.BindsInstance
import dagger.Component


@Component(
    modules = [AppModule::class, BindBuilderSubcomponentsModule::class]
)
interface AppComponent {

    fun inject(app: App)

    fun inject(mainActivity: MainActivity)


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun appContext(appContext: App): Builder

        @BindsInstance
        fun carsRepository(carsRepository: CarRepositoryImpl): Builder

        fun build(): AppComponent
    }
}