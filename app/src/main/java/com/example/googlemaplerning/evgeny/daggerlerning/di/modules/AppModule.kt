package com.example.googlemaplerning.evgeny.daggerlerning.di.modules

import android.content.Context
import com.example.googlemaplerning.evgeny.daggerlerning.App
import com.example.googlemaplerning.evgeny.daggerlerning.di.scopes.AppScopes
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepository
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepositoryImpl
import dagger.Binds
import dagger.Module


@Module
interface AppModule {

    @Binds
    fun bindAppContext(app: App): Context

    @Binds
    fun bindCarsRepository(carsRepository: CarRepositoryImpl): CarRepository

}