package com.example.googlemaplerning.evgeny.daggerlerning.di.modules

import com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components.FragmentSunComponent
import dagger.Module

@Module(subcomponents = [FragmentSunComponent::class])
interface BindBuilderSubcomponentsFragmentModule