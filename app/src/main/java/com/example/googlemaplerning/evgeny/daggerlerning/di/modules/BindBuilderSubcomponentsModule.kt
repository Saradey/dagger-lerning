package com.example.googlemaplerning.evgeny.daggerlerning.di.modules

import com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components.ActivitySubComponent
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepositoryImpl
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainActivity
import dagger.Binds
import dagger.Module
import dagger.Provides


@Module(subcomponents = [ActivitySubComponent::class])
interface BindBuilderSubcomponentsModule