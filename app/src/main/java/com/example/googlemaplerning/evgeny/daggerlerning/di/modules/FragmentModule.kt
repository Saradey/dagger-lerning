package com.example.googlemaplerning.evgeny.daggerlerning.di.modules

import com.example.googlemaplerning.evgeny.daggerlerning.model.Foo
import com.example.googlemaplerning.evgeny.daggerlerning.ui.Adapter
import com.example.googlemaplerning.evgeny.daggerlerning.ui.ContractListener
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainFragment
import dagger.Binds
import dagger.Module
import javax.inject.Singleton


@Module
interface FragmentModule {

    @Binds
    fun bindViewToAdapter(fragment: MainFragment): Adapter.AdapterListener

    @Binds
    fun bindContractListener(fragment: MainFragment): ContractListener


}