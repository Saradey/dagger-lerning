package com.example.googlemaplerning.evgeny.daggerlerning.di.modules

import com.example.googlemaplerning.evgeny.daggerlerning.presenters.FragmentMainPresenter
import com.example.googlemaplerning.evgeny.daggerlerning.presenters.FragmentMainPresenterImpl
import dagger.Binds
import dagger.Module


@Module
interface PresenterModules {

    @Binds
    fun bindPresenter(presenter: FragmentMainPresenterImpl): FragmentMainPresenter

}