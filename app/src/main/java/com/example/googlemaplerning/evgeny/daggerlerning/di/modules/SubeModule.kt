package com.example.googlemaplerning.evgeny.daggerlerning.di.modules

import android.content.Context
import com.example.googlemaplerning.evgeny.daggerlerning.di.scopes.ActivityScopes
import com.example.googlemaplerning.evgeny.daggerlerning.model.Foo
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepository
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepositoryImpl
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainActivity
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainFragment
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton


@Module
class SubeModule {

    @Provides
    fun provideMainActivity(mainActivity: MainActivity): MainFragment.MainFragmentListener =
        mainActivity

    @Provides
    @Named("MainActivity")
    fun provideMainActivitySecond(mainActivity: MainActivity): Context = mainActivity


    @Provides
    fun provideEmployeeRepository(): EmployeeRepository = EmployeeRepositoryImpl()

    @Provides
    @ActivityScopes
    fun provideFoo(): Foo = Foo()

}