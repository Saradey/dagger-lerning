package com.example.googlemaplerning.evgeny.daggerlerning.di.scopes

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScopes