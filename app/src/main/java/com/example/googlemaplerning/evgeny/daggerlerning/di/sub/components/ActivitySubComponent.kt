package com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components

import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.BindBuilderSubcomponentsFragmentModule
import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.PresenterModules
import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.SubeModule
import com.example.googlemaplerning.evgeny.daggerlerning.di.scopes.ActivityScopes
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepositoryImpl
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainActivity
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainFragment
import dagger.BindsInstance
import dagger.Subcomponent
import javax.inject.Singleton


@ActivityScopes
@Subcomponent(
    modules = [
        SubeModule::class,
        PresenterModules::class,
        BindBuilderSubcomponentsFragmentModule::class
    ]
)
interface ActivitySubComponent {

    fun inject(fragment: MainFragment)


    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun bindEmployeeRepository(employeeRepository: EmployeeRepositoryImpl): Builder

        @BindsInstance
        fun bindMainActivity(mainActivity: MainActivity): Builder

        fun build(): ActivitySubComponent
    }

}