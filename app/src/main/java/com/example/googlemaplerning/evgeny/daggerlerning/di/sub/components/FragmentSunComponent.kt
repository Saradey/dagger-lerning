package com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components

import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.FragmentModule
import com.example.googlemaplerning.evgeny.daggerlerning.di.modules.SecondFragmentModule
import com.example.googlemaplerning.evgeny.daggerlerning.presenters.FragmentMainPresenterImpl
import com.example.googlemaplerning.evgeny.daggerlerning.ui.Adapter
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainFragment
import dagger.BindsInstance
import dagger.Subcomponent


@Subcomponent(modules = [FragmentModule::class, SecondFragmentModule::class])
interface FragmentSunComponent {


    fun inject(adapter: Adapter)


    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun bindViewFragment(view: MainFragment): Builder

        fun build(): FragmentSunComponent
    }


}