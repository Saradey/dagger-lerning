package com.example.googlemaplerning.evgeny.daggerlerning.model

import android.util.Log
import javax.inject.Inject

class Foo {

    init {
        Log.d("DaggerDagger", "create Foo")
    }

}