package com.example.googlemaplerning.evgeny.daggerlerning.presenters

import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepository
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepository
import com.example.googlemaplerning.evgeny.daggerlerning.ui.MainFragment
import com.example.googlemaplerning.evgeny.daggerlerning.ui.ViewShow
import javax.inject.Inject


class FragmentMainPresenterImpl @Inject constructor(
    val repository: CarRepository,
    val employeeRepository: EmployeeRepository
) : FragmentMainPresenter {

    var view: ViewShow? = null


    fun attach(view: ViewShow) {
        this.view = view
    }


    fun detach() {
        view = null
    }

}