package com.example.googlemaplerning.evgeny.daggerlerning.ui

import android.content.Context
import com.example.googlemaplerning.evgeny.daggerlerning.model.Foo
import javax.inject.Inject
import javax.inject.Named

class Adapter {


    @Inject
    lateinit var listener: AdapterListener

    @Inject
    lateinit var foo: Foo

    @Inject
    @field:Named("MainActivity")
    lateinit var contextActivity : Context

    @Inject
    lateinit var activity : MainFragment.MainFragmentListener

    init {
        MainFragment.fragmentComponent.inject(this)
    }


    interface AdapterListener
}