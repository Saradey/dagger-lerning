package com.example.googlemaplerning.evgeny.daggerlerning.ui

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.googlemaplerning.evgeny.daggerlerning.App
import com.example.googlemaplerning.evgeny.daggerlerning.R
import com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components.ActivitySubComponent
import com.example.googlemaplerning.evgeny.daggerlerning.model.Foo
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepository
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepositoryImpl
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepositoryImpl
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainFragment.MainFragmentListener {

    companion object {
        lateinit var subeComponent: ActivitySubComponent
    }

    @Inject
    lateinit var builder: ActivitySubComponent.Builder

    @Inject
    lateinit var repository: CarRepository

    @Inject
    lateinit var appContext: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        App.appComponent.inject(this)

        subeComponent = builder
            .bindEmployeeRepository(EmployeeRepositoryImpl())
            .bindMainActivity(this)
            .build()

        supportFragmentManager
            .beginTransaction()
            .add(R.id.frmFiledFragment, MainFragment.getInstance())
            .commit()
    }


}
