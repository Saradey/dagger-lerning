package com.example.googlemaplerning.evgeny.daggerlerning.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.googlemaplerning.evgeny.daggerlerning.R
import com.example.googlemaplerning.evgeny.daggerlerning.di.sub.components.FragmentSunComponent
import com.example.googlemaplerning.evgeny.daggerlerning.model.Foo
import com.example.googlemaplerning.evgeny.daggerlerning.presenters.FragmentMainPresenter
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.CarRepository
import com.example.googlemaplerning.evgeny.daggerlerning.repositorys.EmployeeRepository
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : Fragment(), ViewShow, Adapter.AdapterListener, ContractListener {

//    @Inject
//    lateinit var listener: MainFragmentListener

    @Inject
    lateinit var presenterImpl: FragmentMainPresenter

    @Inject
    lateinit var repository: CarRepository

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var employeeRepository: EmployeeRepository

    @Inject
    lateinit var subFragmentComponent: FragmentSunComponent.Builder

    @Inject
    lateinit var foo: Foo

    lateinit var adapter: Adapter


    companion object {
        fun getInstance(): MainFragment {
            return MainFragment()
        }

        lateinit var fragmentComponent: FragmentSunComponent
    }


    init {
        Log.d("DaggerDagger", "create MainFragment")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_main,
            container,
            false
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.subeComponent.inject(this)
        fragmentComponent = subFragmentComponent.bindViewFragment(this).build()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        txvMainFragment.text = "Проверка на доступность"
        adapter = Adapter()
    }


    interface MainFragmentListener

}